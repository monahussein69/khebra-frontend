import Vue from 'vue';
import Locales from './locale';
import enLocale from 'iview/src/locale/lang/en-US';

const navLang = navigator.language;
const localLang = (navLang === 'en-US') ? navLang : false;
const lang = window.localStorage.lang || localLang || 'en-US';

Vue.config.lang = lang;

const locales = Locales;
const mergeEN = Object.assign(enLocale, locales['en-US']);
Vue.locale('en-US', mergeEN);
