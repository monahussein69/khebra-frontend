import Firebase from 'firebase'

const firebaseApp = Firebase.initializeApp({
  // Populate your firebase configuration data here.
    apiKey: "AIzaSyAu8yiZb_TNJwkUcezeXjmtJ19egsiLnow",
    authDomain: "homefix-205814.firebaseapp.com",
    databaseURL: "https://homefix-205814.firebaseio.com",
    projectId: "homefix-205814",
    storageBucket: "homefix-205814.appspot.com",
    messagingSenderId: "655937709393"
});

// Export the database for components to use.
// If you want to get fancy, use mixins or provide / inject to avoid redundant imports.
export const db = firebaseApp.database();