import Cookies from 'js-cookie';
import util from './util';
import { db } from './../firebase';
import moment from 'moment';
export default {
  errorMessage: function (errors) {
    let message = util.getErrorMessage(errors);
    this.$Message.error(message, 400);
  },
  nameValidation : function(rule, value, callback) {
    (value.match(/[A-z\u0621-\u064A]/g))?'':callback(new Error('Name must contains characters'));
    callback();
  },
  can: function (el, binding, vnode, old) {
    let roles = Cookies.get('roles');
    let is_super = Cookies.get('access') == 1 ? true : false;
    if (!roles && !is_super)
    {
      return false;
    }
    roles = JSON.parse(roles);
    let permission = binding && binding.value && binding.value.permission ? binding.value.permission : el;
    let hide = binding && binding.value && binding.value.hide ? binding.value.hide : true;
    let status = false;
    if (is_super)
    {
      return true;
    }
    if (roles)
    {
      for (var key in roles) {
        let permissions = roles[key];
        status = permissions.filter(permission_code => {
          return permission_code === permission;
        }).length > 0;
      }
    }
    if (!status && hide && vnode)
    {
      let elm = vnode.elm;
      elm.remove( elm );
    }
    return status;
  },
  pushNotification : function(notificationObj){
    let notificationRef = db.ref('notifications/'+notificationObj.user_id);
    notificationRef.push(notificationObj);
  },
  capitalize: function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1);
  },
  toLabel: function (value) {
    if (!value) return ''
    value = value.toString()
    return (value.charAt(0).toUpperCase() + value.slice(1)).replace('_', ' ');
  },

  lastIndex: function (value) {
    if (!value) return ''
    value = value.toString()
    return value.split('\\');
  },
  FormateDate:function(value,formate){
    return moment.unix(value).format(formate);
  }
}
