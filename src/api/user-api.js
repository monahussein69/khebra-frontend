// import axios from 'axios';
import { API_URL, axios } from '../config/constants';

export default {
    login (payload) {
        axios.post(API_URL + 'auth/login', payload.params).then(response => {
            let access_token = 'Bearer ' + response.data.access_token;
            axios.defaults.headers.common['Authorization'] = access_token;
            payload.success && payload.success({ status: true, user: response.data.user, access_token });
        }).catch(error => {
            if (error.response && error.response.status === 401) {
                payload.error && payload.error({ status: false });
            }
        });
    },
    resetPassword (payload) {
        axios.post(API_URL + 'password/reset', payload.params).then(response => {
            payload.success && payload.success({ status: true, message: response.data.message });
        }).catch(error => {
            if (error.response && error.response.status === 401) {
                payload.error && payload.error({ status: false });
            }
            if (error.response && error.response.status === 422) {
                payload.error && payload.error({ status: false});
            }
        });
    },

    forgetPassword (payload) {
        axios.post(API_URL + 'password/email', payload.params).then(response => {
            payload.success && payload.success({ status: true, message: response.data.message });
        }).catch(error => {
            if (error.response && error.response.status === 401) {
                payload.error && payload.error({ status: false });
            }
            if (error.response && error.response.status === 422) {
                payload.error && payload.error({ status: false});
            }
        });
    }
};
