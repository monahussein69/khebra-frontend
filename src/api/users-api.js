// import axios from 'axios';
import { API_URL, axios } from '../config/constants';

export default {
  fetchUsers (payload, cb = null) {
    axios.get(API_URL + 'user?' + payload).then(response => {
        cb && cb(response.data);
    }).catch(errors =>  {
        cb && cb({ status: false });
    });
  },

  fetchUser (id, cb = null) {
      axios.get(API_URL + 'user/' + id + '/edit').then(response => {
          cb && cb(response.data.data);
      }).catch(errors =>  {
        error && error(errors.response.data.errors);
      });
  },

  createUser (payload, cb = null) {
      let { params, success, error } = payload;
      axios.post(API_URL + 'user', params).then(response => {
          success && success(response.data);
          cb && cb(response.data);
      }).catch(errors =>  {
          error && error(errors.response.data.errors);
      });
  },

  updateUser (payload, cb = null) {
      let { params, success, error } = payload;
      axios.put(API_URL + 'user/' + params.id, params).then(response => {
        cb && cb(response.data.data);
          success && success(response.data);
      }).catch(errors =>  {
          error && error(errors.response.data.errors);
      });
  },

  deleteUser (payload) {
      let { params, success, error } = payload;
      axios.delete(API_URL + 'user/' + params.id).then(response => {
          success && success();
      }).catch(errors =>  {
        error && error(errors.response.data.errors);
      });
  }
}
