import usersApi from '../../api/users-api';
import Methods from './../../libs/methods';

const users = {

  state: {
    users: [],
    usersColumns: [],
    usersPaginate: {},
    currentUser: {
      id: null,
      first_name: null,
      last_name: null,
    },
  },

  getters: {
    allUsers: state => state.users,
    usersColumnsList: state => state.usersColumns,
    usersPaginate: state => state.usersPaginate,
    currentUser: state => state.currentUser,
  },

  mutations: {
    setUsers (state, users) {
      state.users = users;
    },
    setCurrentUser (state, user) {
      state.currentUser = user;
    },
    updateUser (state, user) {
      let index = state.users.find((i, index) => {
        if (i.id === user.id){
          return index;
        }
      });
      if (index >= 0) {
        state.users[index] = user;
        state.currentUser = user;
      }
    },
    setUsersPaginate (state, paginate) {
      state.usersPaginate = paginate;
    },
    setUsersColumns (state, columns) {
      state.usersColumns = columns;
    },
    removeUser (state, user_id) {
      state.users.slice(user_id);
    },
    createUser (state, user) {
      state.users.append(user_id);
    }
  },

  actions: {
    getAllUsers ({ commit }, payload = null) {
      usersApi.fetchUsers(payload, users => {
        let { data } = users;
        commit('setUsers', data);
        commit('setUsersPaginate', users);
      });
    },
    getUser ({ commit }, payload) {
      usersApi.fetchUser(payload, user => {
        commit('setCurrentUser', user);
      });
    },
    createUser ({ commit }, payload) {
      usersApi.createUser(payload, user => {
        commit('createUser', user);
      });
    },
    updateUser ({ commit }, payload) {
      usersApi.updateUser(payload, user => {
        commit('updateUser', user);
      });
    },
    createUsersColumns ({ commit }) {
      let columns = [
        { title: '#', type: 'index', width: 80, align: 'center' },
        { title: 'First Name', align: 'center', key: 'first_name' },
        { title: 'Last Name', align: 'center', key: 'last_name' },
        { title: 'Email', align: 'center', key: 'email' },
        { title: 'Actions', align: 'center', key: 'handle', handle: [
          Methods.can('delete-user') ? 'delete' : '',
          Methods.can('edit-user') ? ['Button', {name: 'Edit', action: 'on-click-edit'}] : ''
        ]}
      ];
      commit('setUsersColumns', columns);
    },
    updateAllUsers ({ commit }) {

    },
    deleteUser ({ commit }, payload) {
      usersApi.deleteUser(payload, user => {
        commit('deleteUser', payload);
      });
    },
  }
}

export default users;
