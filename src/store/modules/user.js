import Cookies from 'js-cookie';
import userApi from '../../api/user-api';
import Methods from './../../libs/methods';

const user = {
    state: {
        user: {}
    },

    getter: {
        user: state => state.user
    },

    mutations: {
        setUser (state, user) {
            state.user = user;
        },
        logout (state, vm) {
            Cookies.remove('user');
            Cookies.remove('password');
            Cookies.remove('access');
            Cookies.remove('roles');
            Cookies.remove('userId');
            let themeLink = document.querySelector('link[name="theme"]');
            themeLink.setAttribute('href', '');
            let theme = '';
            if (localStorage.theme) {
                theme = localStorage.theme;
            }
            localStorage.clear();
            if (theme) {
                localStorage.theme = theme;
            }
        }
    },

    actions: {
        login ({ commit }, payload) {
            userApi.login(payload).then((response) => {
                commit('setUser', response);
            });
        },
        resetPassword ({ commit }, payload) {
            userApi.resetPassword(payload).then((response) => {

            });
        },
        forgetPassword ({ commit }, payload) {
            userApi.forgetPassword(payload).then((response) => {
            });
        }

    }
};

export default user;
