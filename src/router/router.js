import Main from '@/views/Main.vue';

export const loginRouter = {
  path: '/login',
  name: 'login',
  meta: {
    title: 'Login - page'
  },
  component: () => import('@/views/login/login.vue')
};

export const resetPasswordRouter = {
  path: '/resetPassword/:token',
  name: 'resetpassword',
  meta: {
    title: 'Login - reset password'
  },
  component: () => import('@/views/login/resetPassword.vue')
};

export const forgetPasswordRouter = {
  path: '/forgetPassword',
  name: 'forgetPassword',
  meta: {
    title: 'Login - Forget password'
  },
  component: () => import('@/views/login/forgetPassword.vue')
};

export const locking = {
  path: '/locking',
  name: 'locking',
  component: () => import('@/views/main-components/lockscreen/components/locking-page.vue')
};

export const otherRouter = {
  path: '/',
  name: 'otherRouter',
  redirect: '/home',
  component: Main,
  children: [
    { path: 'home', title: {i18n: 'home'}, name: 'home_index', component: () => import('@/views/home/home.vue') },  ]
};

export const appRouter = [
  {
    path: '/',
    name: 'dashboard',
    icon: 'ios-toggle',
    redirect: '/home',
    component: Main,
    children: [
      { path: 'Dashboard', title: {i18n: 'Dashboard'}, access: 0, name: 'dashboard_index', component: () => import('@/views/home/home.vue') },
    ]
  },

  {
    path: '/master-data',
    name: 'master-data',
    icon: 'home',
    title: 'Master Data',
    component: Main,
    children: [

    ]
  },
];

export const errorRouter = [
  {
    path: '/500',
    meta: { title: '500-服务端错误' },
    name: 'error-500',
    component: () => import('@/views/error-page/500.vue')
  },
  {
    path: '/403',
    meta: { title: '403-权限不足' },
    name: 'error-403',
    component: () => import('@/views/error-page/403.vue')
  },
  {
    path: '/404',
    meta: { title: '404-页面不存在' },
    name: 'error-404',
    component: () => import('@/views/error-page/404.vue')
  },
];

let routersToLoop = [
  loginRouter,
  resetPasswordRouter,
  forgetPasswordRouter,
  otherRouter,
  ...errorRouter,
  ...appRouter,
  locking,
];

export const routers = [
  loginRouter,
  resetPasswordRouter,
  forgetPasswordRouter,
  otherRouter,
  ...errorRouter,
  ...appRouter,
  locking,
];
