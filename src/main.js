import Vue from 'vue';
import iView from 'iview';
import {router} from './router/index';
import {appRouter} from './router/router';
import store from './store';
import App from './app.vue';
import 'iview/dist/styles/iview.css';
import '@/locale';
import VueI18n from 'vue-i18n';
import util from './libs/util';
import locale from 'iview/dist/locale/en-US';
import * as VueGoogleMaps from 'vue2-google-maps'
import VueMoment from 'vue-moment'
import Mixins from './libs/mixins.js';
import Methods from './libs/methods.js';
import Firebase from 'firebase';
import VueFire from 'vuefire';
import FullCalendar from 'vue-full-calendar';
import "fullcalendar/dist/fullcalendar.min.css";

Vue.use(VueI18n);
Vue.use(iView, {locale: locale});
Vue.use(VueMoment);
Vue.use(VueFire);
Vue.use(FullCalendar);


Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyC--EfdkW3L9gfxq3zMADlRpoTgvAcmcnc",
    libraries: "places"
  }
});
Vue.mixin(Mixins);
Vue.directive('can', Mixins.methods.can)
Vue.filter('capitalize', Methods.capitalize)
Vue.filter('toLabel', Methods.toLabel)
Vue.filter('lastIndex', Methods.lastIndex)



new Vue({
  el: '#app',
  router: router,
  store: store,
  render: h => h(App),
  data: {
    currentPageName: '',
  },
  mounted () {
    this.currentPageName = this.$route.name;
    // 显示打开的页面的列表
    this.$store.commit('setOpenedList');
    this.$store.commit('initCachepage');
    // 权限菜单过滤相关
    this.$store.commit('updateMenulist');
    // iview-admin检查更新
    // util.checkUpdate(this);
  },
  created () {
    let tagsList = [];
    appRouter.map((item) => {
      if (item.children.length <= 1) {
        tagsList.push(item.children[0]);
      } else {
        tagsList.push(...item.children);
      }
    });
    this.$store.commit('setTagsList', tagsList);
  }
});
