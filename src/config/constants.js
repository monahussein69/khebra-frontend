import { API_BASE_URL, BASE_URL } from './dev';
import vue_axios from 'axios'
import Cookies from 'js-cookie';

export const API_URL = API_BASE_URL + 'api/front/v1/';
export const STORAGE_URL = BASE_URL + 'storage/uploads/';
export const STATIC_IMAGES_URL = BASE_URL + 'storage/default/';
export const EMPTY_IMAGE_URL = STATIC_IMAGES_URL + 'default.png';
export const USER_EMPTY_IMAGE_URL = STATIC_IMAGES_URL + 'default_user.jpg';
export const NATIONALITYS = {'saudi': 'Saudi', 'from_a_saudi_mother': 'From a Saudi mother', 'born_in_saudi_arabia': 'Born in Saudi Arabia'};
export const axios = vue_axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
    'Authorization': Cookies.get('authorization')
  }
})
